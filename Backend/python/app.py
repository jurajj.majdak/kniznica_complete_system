from flask import Flask, render_template, request, jsonify
import mysql.connector
from flask_cors import CORS 
from flaskext.mysql import MySQL


app = Flask(__name__)
CORS(app)

@app.route('/index.html')

def domov():

	return render_template('index.html')

@app.route('/book.html')
def kniha():
    return render_template('book.html')
   
@app.route('/login.html')
def login():
    return render_template('login.html')

@app.route('/profil1.html')
def profil1():
    return render_template('profil1.html')


@app.route('/profil.html')
def profil():
    return render_template('profil.html')

@app.route('/registracia.html')
def registracia():
    return render_template('registraca.html')

@app.route('/search.html')
def search():
    return render_template('search.html')

@app.route('/zakaznici')
def zakaznik():
    from zakaznici import zakaznici
    zakaznik = zakaznici
    return zakaznici()

if __name__ == '__main__':

	app.run(host="0.0.0.0")
	app.run(debug=True)
	app.run(reloader=True)
