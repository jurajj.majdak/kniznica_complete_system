from flask import Flask, jsonify, request
from flaskext.mysql import MySQL
import mysql.connector
import json

app = Flask(__name__)

mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'Kniznica'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

def zakaznici():
	conn = mysql.connect()
	cursor =conn.cursor()

	cursor.execute('''
        SELECT zakaznik.id_zakaznik, osoba.meno, osoba.priezvisko from zakaznik 
        inner JOIN osoba on zakaznik.id_zakaznik = osoba.id_zakaznik
        ''')
	data = cursor.fetchall()



	json_output = json.dumps(data)

	return jsonify(data)


