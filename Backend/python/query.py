from flask import Flask, render_template, request
from flaskext.mysql import MySQL
import mysql.connector
import json


app = Flask(__name__)

mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'Kniznica'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

@app.route('/')
def skuska():
	conn = mysql.connect()
	cursor =conn.cursor()

	cursor.execute("SELECT * from kniha")
	data = cursor.fetchall()



	json_output = json.dumps(data)

	return json_output

if __name__ == '__main__':
    app.run()
